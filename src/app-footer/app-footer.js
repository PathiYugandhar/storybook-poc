import React from 'react';
import { Box, Text, Anchor } from 'grommet';
import { PropTypes } from 'prop-types';

const AppFooter = ({ hpeCopyRightLabel, dataTestId, footerLinks }) => {
  return (
    <Box
      align='center'
      direction='row'
      justify='between'
      data-testid={dataTestId}
    >
      <Text label={hpeCopyRightLabel} color='gray'>
        {hpeCopyRightLabel}
      </Text>
      <Box direction='row' gap='small'>
        {footerLinks.map((link) => (
          <Anchor href={link.url} size='small'>
            {link.label}
          </Anchor>
        ))}
      </Box>
    </Box>
  );
};

AppFooter.propTypes = {
  dataTestId: PropTypes.string,
  footerLinks: PropTypes.arrayOf({
    label: PropTypes.string,
    url: PropTypes.string,
  }),
};

export default AppFooter;
