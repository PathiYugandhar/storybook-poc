import React from 'react';

import AppFooter from './app-footer';

export default {
  title: 'CCS / AppFooter',
  component: AppFooter,
};

const Template = (args) => <AppFooter {...args} />;

export const PageFooter = Template.bind({});
PageFooter.args = {
  hpeCopyRightLabel: 'Hewlett Packard Enterprise Development LP',
  dataTestId: 'page-footer',
  footerLinks: [
    { label: 'Help', url: '/' },
    { label: 'Terms', url: '/' },
    { label: 'Privacy', url: '/' },
    { label: 'Security', url: '/' },
    { label: 'Feedback', url: '/' },
  ],
};
