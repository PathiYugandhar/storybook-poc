import React from 'react';

import AppHeader from './app-header';

export default {
  title: 'CCS / AppHeader',
  component: AppHeader,
};

const Template = (args) => <AppHeader {...args} />;

export const PlainHeader = Template.bind({});
PlainHeader.args = {
  heading: 'Hewlett Packard Enterprise',
};

export const CloudHomePage = Template.bind({});
CloudHomePage.args = {
  appName: 'Cloud',
};

export const ManageAccountApp = Template.bind({});
ManageAccountApp.args = {
  appName: 'Manage Account',
};
