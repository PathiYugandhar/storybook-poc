import React from 'react';
import { Box, Text } from 'grommet';
import { Hpe } from 'grommet-icons';

import PropTypes from 'prop-types';

const AppHeader = ({ heading, appName, hpeLabel, dataTestId }) => {
  return (
    <Box direction='row' align='center' gap='xsmall' data-testid={dataTestId}>
      <Hpe size='large' color='brand' />
      {heading && <Text>{heading}</Text>}
      {appName && (
        <Box direction='row' gap='xsmall'>
          <Text weight='bold'>HPE</Text>
          <Text>{appName}</Text>
        </Box>
      )}
    </Box>
  );
};

AppHeader.propTypes = {
  /**
   * Header string Ex: Hewlett Packard Enterprise
   */
  heading: PropTypes.string,
  /**
   * Application which is currently active. Ex: Cloud, Manage Account
   */
  appName: PropTypes.string,
  /**
   * hpe string localized
   */
  hpeLabel: PropTypes.string,
  /**
   * data-testid used for unit testing and cypress tests
   */
  dataTestId: PropTypes.string,
};

AppHeader.defaultProps = {
  heading: null,
  appName: null,
  hpeLabel: 'HPE',
};

export default AppHeader;
