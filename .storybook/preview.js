import React from 'react';
import { hpe } from 'grommet-theme-hpe';
import { Grommet } from 'grommet';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};

export const decorators = [
  (Story) => (
    <Grommet theme={hpe}>
      <Story />
    </Grommet>
  ),
];
